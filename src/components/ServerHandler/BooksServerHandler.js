import { AsyncStorage } from 'react-native';
import { USER_KEY, SERVER } from '../../utils/config';

class filename {
    
    /**
     * Calls API to upload a new book
     */
    static uploadBook = async (bookObject) => {

        bookObject['userEmail'] = await AsyncStorage.getItem(USER_KEY);
        console.log(bookObject);
        const object = {

            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            
            body: JSON.stringify({book_object: bookObject})
        }

        const responseJson = await fetch(SERVER.books, object)
        .then((response) => {

            if (response.headers.get('content-type').match(/application\/json/)) {
                return response.json();
            }

            return response;
        });
        
        if (responseJson['id'] != null) {

            return true;
        }
        else {

            return false;
        }
    }


    static async getECommerceInfo(isbn) {

        return fetch(`https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&jscmd=details&format=json`)
        .then((response) => response.json())
        .catch((error) =>{
            console.error(error);
        });
    }


    static async searchBook(param) {

        const url = `${SERVER.searchBook}${param}`;

        return fetch(url).then(res => res.json());
    }

    /**
     * Calls API to get all the books for this user
     */
    static async retrieveBooks() {

        const url = `${SERVER.books}/${await AsyncStorage.getItem(USER_KEY)}`;

        return fetch(url)
        .then(res => res.json())
    };

    /**
     * Calls Books DB API to get info about book
     */
    static retrieveBookInfo = async (isbn) => {

        return fetch(`https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&jscmd=details&format=json`)
        .then((response) => response.json())
        .catch((error) =>{
            console.error(error);
        });
    }
}

export default filename;
