import React from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import SocketIOClient from 'socket.io-client';
import { View, Text, StyleSheet,Button,Header } from 'react-native';

class ChatRoom extends React.Component {

  constructor(props) {
    super(props);
    this.scheduleMeeting = this.scheduleMeeting.bind(this);

    this.state = {
        messages: [],
      };
    this.load = this.load.bind(this);
    this.socket = SocketIOClient('http://localhost:80');
    this.socket.on('newMessageCreated', (message) => {
      let newMessages=message
      .map((message)=>{
        let name= (message.id==ConvoData.senderId)?ConvoData.sellerName:ConvoData.buyerName;
          return  {
            _id: message.id,
            text: message.text,
            createdAt: new Date(),
            user: {
                _id: message.senderId,
                name: name,
              }
          }
        })
  this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, newMessages),
    }))
})
  }
  
  componentDidMount(){
      console.log(this.props.navigation)
      this.load();
  }

  sendMessage(text, senderId, receiverId,id){
      this.socket.emit('newMessage',{
          senderId:senderId,
          recieverId:receiverId,
          dateCreated:new Date(),
          converstionId:id,
          text:text} );
  }

  load (){
   let ConvoData=this.props.navigation.state.params.conversationObject;
   this.setState({
    messages:[
      {
        "id": "fff",
        "senderId": "dfd",
        "recieverId": "ddd",
        "dateCreated": "12/03/18",
        "converstionId": "vhb",
        "text": "hey"
      },
      {
        "id": "ffs",
        "senderId": "ddd",
        "recieverId": "dfd",
        "dateCreated": "12/03/18",
        "converstionId": "vhb",
        "text": "hey"
      }
      ].map((message)=>{
      let name= (message.id==ConvoData.senderId)?ConvoData.sellerName:ConvoData.buyerName;
        return  {
          _id: message.id,
          text: message.text,
          createdAt: new Date(),
          user: {
              _id: message.senderId,
              name: name,
            }
        }
      })})
  }

  onSend(message=[]) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, message),
    }))
   let conversationObject = this.props.navigation.state.params.conversationObject;
   this.sendMessage(message.text, conversationObject.user, conversationObject.receiver, conversationObject.id);
  }

  scheduleMeeting(){
    let convoObj=this.props.navigation.state.params.conversationObject;
    this.props.navigation.navigate('ScheduleAppointment', {
      subjectObject: convoObj
  });
  }


  render() {
      return (
        <View style={{ flex: 1 }}>
        
        <Button title={"SChedule in person meeting"} onPress={this.scheduleMeeting} />
        <GiftedChat
          messages={ this.state.messages}
          onSend={ (messages) => this.onSend(messages)}
          user={{
            _id: 1,
          }}
        />
        </View>
      );
  }
}

export default ChatRoom;
