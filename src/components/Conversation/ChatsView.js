import React, {
    Component
} from 'react';
import {
    View,
    StyleSheet
} from 'react-native';
import {
    List,
    ListItem
} from "react-native-elements";
import {
    FlatList,
    ActivityIndicator
} from "react-native";

class filename extends Component {

    constructor() {
        super();

        this.openChat = this.openChat.bind(this);

        this.state = {
            loading: false,
            data: [],
            error: null,
            refreshing: false
        };

    }

    componentWillMount() {
        this.data = this.getConversation()
    }

    getConversation() {
        conversations = [
            {
              "buyerId": "dfd",
              "sellerId": "ddd",
              "sellerName": "tom",
              "buyerName": "dave",
              "type": "conversation",
              "conversationId": "vhb"
            }
          ]
        return conversations;
    }

    handleRefresh = () => {
        this.setState({
                refreshing: true
            },
            () => {
                //refresh method here
            }
        );
    };

    openChat(conversation) {
        this.props.navigation.navigate('chatRoom', {
            conversationObject: conversation
        });
    }

    renderSeparator = () => {
        return ( <View style={
                {
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }
            }/>
        );
    };

    renderFooter = () => {
        if (!this.state.loading) return null;

        return ( <View style = {
                {
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }
            }>
            <ActivityIndicator animating size = "large"/>
            </View>
        );
    };



    render() {
        return ( <View>
            <List containerStyle = {
                {
                    borderTopWidth: 0,
                    borderBottomWidth: 0
                }
            } >
            <FlatList data = {
                this.data
            }
            renderItem = {
                ({
                    item
                }) => ( <ListItem onPress = {() => this.openChat(item)}
                    title = {
                        item.sellerName
                    }
                    subtitle = {
                        item.sellerId
                    }
                    containerStyle = {
                        {
                            borderBottomWidth: 0
                        }
                    } />
                )
            }
            keyExtractor = {
                item => item.email
            }
            ItemSeparatorComponent = {
                this.renderSeparator
            }
            ListHeaderComponent = {
                this.renderHeader
            }
            ListFooterComponent = {
                this.renderFooter
            }
            onRefresh = {
                this.handleRefresh
            }
            refreshing = {
                this.state.refreshing
            }
            onEndReached = {
                this.handleLoadMore
            }
            onEndReachedThreshold = {
                50
            }/>
             </List>
              </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});
export default filename;