import React, {
    Component
} from 'react';
import {
    View,Text,  TouchableOpacity,
    StyleSheet
} from 'react-native';
import {
    List,
    ListItem
} from "react-native-elements";
import {
    FlatList,
    ActivityIndicator,Button
} from "react-native";
import { CheckBox } from 'react-native-elements'
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import RNCalendarEvents from 'react-native-calendar-events';
import SocketIOClient from 'socket.io-client';


import DateTimePicker from 'react-native-modal-datetime-picker';



class ScheduleAppointment extends Component {
    state = {
        seller:"",
        isDateTimePickerVisible: false,
        day_before:false,
        thirty_before:false,
        one_hour_before:false,
        time:new Date()
      };
     
      _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    
      _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    
      _handleDatePicked = (date) => {
        this.setState((previousState) => {
           return {time: new Date(date),}
          })
        this._hideDateTimePicker();
      };
    
    constructor() {
        super();
        this.scheduleMeeting = this.scheduleMeeting.bind(this);
       console.log(RNCalendarEvents.authorizationStatus())
        RNCalendarEvents.authorizeEventStore()
        this.socket = SocketIOClient('http://localhost:80');
   

    }

    componentWillMount() {
this.state.seller=this.props.navigation.state.params.subjectObject.sellerName;
    }

scheduleMeeting(){
let title="in person meeting with "+this.state.seller
let end=this.state.time.setHours(23,59,59,999)
let alarm=[]
if(this.state.day_before){alarm.push({date:24*60})}
if(this.state.thirty_before){alarm.push({date:30})}
if(this.state.one_hour_before){alarm.push({date:60})}
let rs=RNCalendarEvents.saveEvent(title, {
    startDate: this.state.time.toISOString(),
    endDate: new Date(end),
   alarms:alarm
  }) 
  
  let persistentAppointment={
  buyerId:this.props.navigation.state.params.subjectObject.buyerId,
  sellerId:this.props.navigation.state.params.subjectObject.sellerId,
  buyerName:this.props.navigation.state.params.subjectObject.buyerName,
  sellerName:this.props.navigation.state.params.subjectObject.sellerName,
  location:"default",
  time:this.state.time.toISOString(),
  approved:false}
  this.socket.emit("ScheduledANewAppointment",persistentAppointment)
  alert("Successfully set up an appointment with "+ this.state.seller+" on "+this.state.time.toLocaleDateString())

}



    render() {
        return ( 
           


<View style={{ flex: 1 }}>
<Text h1>Setup an appointment with {this.state.seller}</Text>
<CheckBox
center
title='Schedule a reminder 24 hours before the meeting'
checkedIcon='dot-circle-o'
uncheckedIcon='circle-o'
onPress={() => this.setState((state)=>{
    return {day_before: !this.day_before}
  })}
checked={this.state.day_before}
/>

<CheckBox
center
title='Schedule a reminder 30 minutes before the meeting'
checkedIcon='dot-circle-o'
uncheckedIcon='circle-o'
onPress={() => this.setState({
    thirty_before: !this.thirty_before
  })}
checked={this.state.thirty_before}
/>

<CheckBox
center
title='Schedule a reminder an hour before the meeting'
checkedIcon='dot-circle-o'
uncheckedIcon='circle-o'
onPress={() => this.setState({
    one_hour_before: !this.one_hour_before
  })}
checked={this.state.one_hour_before}
/>
  <Text>{this.state.time.toDateString()}</Text>
<Button title={"Select a date"} onPress={this._showDateTimePicker} />

<DateTimePicker
is24Hour={true}
mode={"datetime"}
  isVisible={this.state.isDateTimePickerVisible}
  onConfirm={this._handleDatePicked}
  onCancel={this._hideDateTimePicker}
/>
<Button title={"Save"} onPress={this.scheduleMeeting} />

</View>






        );
    }
}


export default ScheduleAppointment;