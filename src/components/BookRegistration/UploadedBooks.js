import React, { Component } from "react";
import { View, FlatList, ActivityIndicator } from "react-native";
import { List, ListItem, Button } from "react-native-elements";
import BooksServerHandler from '../ServerHandler/BooksServerHandler';

class filename extends Component {
  
  constructor(props) {
    super(props);

    this.loadBooksData = this.loadBooksData.bind(this);

    this.state = {
      loading: false,
      data: [],
      error: null,
      refreshing: false
    };
  }
   
  navigationButtonPressed = () => {

    this.props.navigation.navigate('uploadNewBook', {callBack: this.loadBooksData});
  }

  componentWillMount() {
    this.loadBooksData();
    this.props.navigation.setParams({ navigate: this.navigationButtonPressed });
  }

  static navigationOptions =  ({ navigation }) => {
    return {

      headerTintColor: '#fff',
      headerRight: (
        <Button
          onPress={navigation.getParam('navigate')}
          title="+"
        />
      )
    }
  };

  /**
   * Loads all the books and populates the list view
   */
  loadBooksData = async () => {
    
    this.setState({ loading: true });

    BooksServerHandler.retrieveBooks()
    .then(res => {
      console.log(res);
      this.setState({
        data: res,
        error: res.error || null,
        loading: false,
        refreshing: false
      });
    })
    .catch(error => {
      this.setState({ error, loading: false });
    });
  }

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => {
        this.loadBooksData();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      () => {
        this.loadBooksData();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };
  
  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
        <View>
            <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
                <FlatList
                data={this.state.data}
                renderItem={({ item }) => (
                    <ListItem
                    roundAvatar
                    title={`${item.title} ${item.year}`}
                    subtitle={item.retailPrice}
                    containerStyle={{ borderBottomWidth: 0 }}
                    />
                )}
                keyExtractor={item => item.email}
                ItemSeparatorComponent={this.renderSeparator}
                ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                onRefresh={this.handleRefresh}
                refreshing={this.state.refreshing}
                onEndReached={this.handleLoadMore}
                onEndReachedThreshold={50}
                />
            </List>
        </View>
    );
  }
}

export default filename;
