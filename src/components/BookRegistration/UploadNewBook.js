import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Button } from 'react-native';
import { Sae } from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Rating } from 'react-native-elements';
import BooksServerHandler from '../ServerHandler/BooksServerHandler';


class filename extends Component {

    constructor(props) {
        super(props);

        this.state = {
            rateText:'Acceptable',
            rateColor: 'gray',
            bookObject: {
                quality: 1,
                title: '',
                description: '',
                isbn10: '',
                isbn13: '',
                retailPrice: '',
                year: '',
                edition: '',
                publisher: '',
                author: ''
            }
        }

        this.fillBookInfo = this.fillBookInfo.bind(this);
        this.setBookQuality = this.setBookQuality.bind(this);
        this.onRatingStarted = this.onRatingStarted.bind(this);
        this.onRatingCompleted = this.onRatingCompleted.bind(this);
        this.onUploadBook = this.onUploadBook.bind(this);
    }

    componentWillMount() {

        this.props.navigation.setParams({ navigate: this.navigationButtonPressed });
    }

    navigationButtonPressed = () => {
        
        this.props.navigation.navigate('barcodeScan', {callBack: this.fillBookInfo});
    }

    static navigationOptions =  ({ navigation }) => {
        return {
    
          headerTintColor: '#fff',
          headerRight: (
            <Button
              onPress={navigation.getParam('navigate')}
              title="scan"
            />
          )
        }
      };

    onChangeText = (key, value) => {
        
        const bookObject = Object.assign({}, this.state.bookObject, { [key]: value })
        this.setState({ bookObject });
    }

    /**
     * Sets book's quality when user drags a rating view
     */
    setBookQuality = (value) => {

        if (value == 1 || value == 0) this.setState({rateText: "Acceptable", rateColor: "gray"}) 
        else if (value == 2) this.setState({rateText: "Good", rateColor: "yellow"}) 
        else if (value == 3) this.setState({rateText: "Excellent", rateColor: "green"}) 

        const bookObject = Object.assign({}, this.state.bookObject, { ['quality']: value })
        this.setState({ bookObject });
    }

    onRatingStarted(value) {

        this.setBookQuality(value);
    }

    onRatingCompleted(value) {

        this.setBookQuality(value);
    }

    /**
     * Uploads a new books and pops to the list of the uploaded books
     */
    onUploadBook() {
        
        if (BooksServerHandler.uploadBook(this.state.bookObject)) {
            
            this.props.navigation.state.params.callBack();
            this.props.navigation.pop();
        }
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.cards}>
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'Book title'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('title', val)}
                    value={this.state.bookObject.title}
                />
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'Short description'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('description', val)}
                    value={this.state.bookObject.description}
                />
                <View style={styles.rating}>
                    <Rating
                        type="rocket"
                        ratingCount={3}
                        startingValue={1}
                        imageSize={40}
                        onFinishRating={this.onRatingCompleted}
                        onStartRating={this.onRatingStarted}
                    />
                    <Text style={styles.quality}>Quality:</Text>
                    <Text style={[styles.quality, {color: this.state.rateColor}]}>{this.state.rateText}</Text>
                </View>
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'ISBN 10'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('isbn10', val)}
                    value={this.state.bookObject.isbn10}
                />
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'ISBN 13'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('isbn13', val)}
                    value={this.state.bookObject.isbn13}
                />
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'Price'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('retailPrice', val)}
                    value={this.state.bookObject.retailPrice}
                />
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'Year published'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('year', val)}
                    value={this.state.bookObject.year}
                />
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'Edition'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('edition', val)}
                    value={this.state.bookObject.edition}
                />
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'Pusblisher'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('publisher', val)}
                    value={this.state.bookObject.publisher}
                />
                <Sae
                    labelStyle={styles.label}
                    inputStyle={styles.inputText}
                    style={styles.input}
                    label={'Book Author'}
                    iconClass={FontAwesomeIcon}
                    iconName={'pencil'}
                    iconColor={'#3498db'}
                    onChangeText={val => this.onChangeText('author', val)}
                    value={this.state.bookObject.author}
                />
                <TouchableOpacity style={styles.buttonContainer} onPress={this.onUploadBook}>
                    <Text style={styles.buttonText}>Add Book</Text>
                </TouchableOpacity>
                </View>
            </ScrollView>

        );
    }

    static get options() {
        return {
          topBar: {
            rightButtons: [
                {
                //   icon: require('../../../assets/icons/scan.png'),
                  id: 'scanBookIsbn'
                },
              ]
          }
        };
      }
    

    /**
     * Fills book's fields if user scaned ISBN with camera
     * @param {*} isbn 
     */
    async fillBookInfo(isbn) {

        BooksServerHandler.retrieveBookInfo(isbn)
        .then((responseJson) => {

            details = responseJson[`ISBN:${isbn}`]['details'];

            const bookObject = Object.assign({}, this.state.bookObject, { 
                
                ['title']: details['title'] ? details['title'] : '',
                ['publisher']: details['publishers'] ? details['publishers'][0] : '',
                ['author']: details['authors'] ? details['authors'][0]['name'] : '',
                ['isbn10']: details['isbn_10'] ? details['isbn_10'][0] : '',
                ['isbn13']: details['isbn_13'] ? details['isbn_13'][0] : '',
                ['year']: details['publish_date'] ? details['publish_date'] : ''
            })

            this.setState({ bookObject });
        })
    }

    /**
     * Navigates to the barcode scan view
     * @param {*} param0 
     */
    navigationButtonPressed({ buttonId }) {
        
        if (buttonId === 'scanBookIsbn') {
    
            // Navigation.push(this.props.componentId, {
            //     component: {
            //         name: 'BarcodeScan',
            //         options: { bottomTabs: { visible: false, drawBehind: true, animate: true } },
            //         passProps: {onDone: this.fillBookInfo} 
            //     }
            // });
        }
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 10,
      backgroundColor: 'white',
    },
    label: {
        color: '#3498db'
    },
    inputText: {
        color: 'gray'
    },  
    content: {
      paddingBottom: 300,
    },
    cards: {
      padding: 16,
    },
    input: {
      marginTop: 4,
    },
    rating: {
        marginTop: 20,
        flexDirection: 'row'
    },
    quality: {
        marginLeft: 10,
        fontSize: 20,
        alignSelf: "center",
        fontWeight: 'bold',
        color: '#3498db'
    },
    buttonContainer: {
        marginTop: 40,
        backgroundColor: '#2980b9',
        paddingVertical: 10,
        marginBottom: 10
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700'
    }
  });

export default filename;
