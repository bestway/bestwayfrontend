import React, {Component} from 'react';
import {
  View,
  StyleSheet
} from 'react-native';
import Camera from 'react-native-camera';

class filename extends Component {
    
    render () {
        return (
            <View style={styles.container}>
                <Camera
                    style={styles.preview}
                    onBarCodeRead={this.onBarCodeRead.bind(this)}
                    ref={cam => this.camera = cam}
                    aspect={Camera.constants.Aspect.fill}>
                    <View style={styles.rectangleContainer}>
                        <View style={styles.rectangle}/>
                    </View> 
                </Camera>
            </View>
        )
    }

    /**
     * Scans bar code and makes a callback to the previous view
     */
    onBarCodeRead = (data) => {

        this.props.navigation.state.params.callBack(data.data);
        this.props.navigation.goBack();
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    rectangleContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },
    
    rectangle: {
        height: 250,
        width: 250,
        borderWidth: 2,
        borderColor: '#00FF00',
        backgroundColor: 'transparent',
    }
  });

export default filename;
