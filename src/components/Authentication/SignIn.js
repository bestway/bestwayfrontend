import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  AsyncStorage,
  StatusBar, 
  KeyboardAvoidingView, 
  TouchableOpacity
} from 'react-native'

import { USER_KEY, SERVER } from '../../utils/config'

export default class SignIn extends React.Component {

    state = {
        email: '', password: ''
    }

    onChangeText = (key, value) => {
        this.setState({ [key]: value })
    }

    signIn = async () => {
       
        try {
            const object = {

                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                
                body: JSON.stringify( {
                    'email': this.state.email,
                    'password': this.state.password
                })
            }
            const responseJson = await fetch(SERVER.user, object)
            .then((response) => {

                if (response.headers.get('content-type').match(/application\/json/)) {
                    return response.json();
                }

                return response;
            });
                
            if (responseJson['email'] != null) {

                try {
                    const user = await AsyncStorage.setItem(USER_KEY, responseJson['email'])
                    this.props.navigation.navigate('App');
                } 
                catch (err) {}
            }
        }
        catch (error) {
            console.error(error);
        }
  }
  render() {
    return (
        <View style={styles.wrapper}>
                <StatusBar 
                    barStyle='dark-content'
                />
                <View style={styles.titleWrapper}>
                    <Text style={styles.title}>Best Way</Text>
                </View>
                <KeyboardAvoidingView behavior='padding' style={styles.container}>
                <TextInput 
                    placeholder='email'
                    returnKeyType='next'
                    onSubmitEditing={() => this.passwordInput.focus()}
                    keyboardType='email-address'
                    autoCapitalize='none'
                    onChangeText={val => this.onChangeText('email', val)}
                    style={styles.input}>
                </TextInput>
                <TextInput 
                    placeholder='password'
                    returnKeyType='go'
                    secureTextEntry
                    ref={(input) => this.passwordInput = input}
                    onChangeText={val => this.onChangeText('password', val)}
                    style={styles.input}>
                </TextInput>
                <TouchableOpacity style={styles.buttonContainer} onPress={this.signIn}>
                    <Text style={styles.buttonText}>Sign In</Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: "#3498db",
        flex: 1,
    },
    title: {
        color: 'white',
        alignSelf: 'center',
        fontSize: 35,
        fontWeight: 'bold'
    },
    titleWrapper: {
        justifyContent: 'center',
        flex: 1
    },
    container: {
        padding: 20,
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 20,
        color: '#FFF',
        paddingHorizontal: 10
    },
    buttonContainer: {
        backgroundColor: '#2980b9',
        paddingVertical: 10,
        marginBottom: 10
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700'
    },
    subtitle: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: '200',
        paddingBottom: 30
    },
})