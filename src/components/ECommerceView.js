import React, { Component } from 'react';
import { WebView, StyleSheet } from 'react-native';

class filename extends Component {
    render() {
        return (
            <WebView
                source={{uri: this.props.navigation.state.params.bookObject.publisher}}
                style={{marginTop: 20}}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

export default filename;
