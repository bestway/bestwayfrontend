import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import { List, ListItem, SearchBar } from 'react-native-elements';
import BooksServerHandler from '../ServerHandler/BooksServerHandler';

class filename extends Component {
  
  constructor(props) {
    super(props);

    this.searchFilterFunction = this.searchFilterFunction.bind(this);
    this.onViewBook = this.onViewBook.bind(this);

    this.state = {
      loading: false,
      data: [],
      error: null,
      searchText: ''
    };

    this.arrayholder = [];
  }

  static navigationOptions = {
    header: null
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  onViewBook(book) {

    if (book.type) {

      this.props.navigation.navigate('eCommerceView', {bookObject: book});
    }
  }

  searchFilterFunction = (text) => {
    
    if (text.length == 0) {

        this.setState({
            data: [],
            error: null,
            loading: false,
        });
    }
    else {

        this.setState({ loading: true });

        this.setState({
          data: [],
          error: null,
          loading: false,
        });

        BooksServerHandler.searchBook(text)
        .then(result => {

            var books = [];

            result.forEach((book) => {

              books.push(book);

              BooksServerHandler.getECommerceInfo(book.isbn10)
              .then(res => {
                
                if (res[`ISBN:${book.isbn10}`]) {
    
                    try {
    
                        let details = res[`ISBN:${book.isbn10}`]['details'];
                        let ecommerce = `https://www.amazon.com/dp/${details['identifiers']['amazon'][0]}`;

                        let ecommerceBook = {

                          title: book.title,
                          id: "qas12",
                          isbn10: book.isbn10,
                          isbn13: book.isbn13,
                          publisher: ecommerce,
                          description: "Amazon Books",
                          type: "ecommerce"
                        }

                        books.push(ecommerceBook);
                    }
                    catch (err) {}
                }
            })

            this.setState({
              data: books,
              error: result.error || null,
              loading: false,
            });
          })
        })
        .catch(error => {
            this.setState({ error, loading: false });
        });
    }
  };

  renderHeader = () => {
    return (
      <SearchBar
        placeholder="Search Your Book..."
        lightTheme
        round
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
      />
    );
  };

  render() {
    return (
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <ListItem
              onPress = {() => this.onViewBook(item)}
              title={item.title}
              subtitle={item.description}
              containerStyle={{ borderBottomWidth: 0 }}
            />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />
      </List>
    );
  }
}

export default filename;