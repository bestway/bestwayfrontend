export const USER_KEY = 'USER_KEY';

let server_url = 'http://localhost:3000';

export const SERVER = {

    user: `${server_url}/api/v1/users`,
    books: `${server_url}/api/v1/books`,
    searchBook: `${server_url}/api/v1/books/search/`
    
}