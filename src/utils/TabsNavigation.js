import React from 'react';
import { Button, Text, View, Image } from 'react-native';
import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import UploadedBooks from '../components/BookRegistration/UploadedBooks';
import BooksSearch from '../components/SearchBook/BooksSearch';
import BooksSelectedToBuy from '../components/BooksSelectedToBuy';
import ChatsView from '../components/Conversation/ChatsView';
import UserSettings from '../components/UserSettings';
import UploadNewBook from '../components/BookRegistration/UploadNewBook';
import BarcodeScan from '../components/BookRegistration/BarcodeScan';
import ChatRoom from "../components/Conversation/ChatRoom";
import ECommerceView from "../components/ECommerceView";
import ScheduleAppointment from "../components/Conversation/scheduleAppointment"
const uploadedBooksStack = createStackNavigator({
  uploadedBooks: UploadedBooks,
  uploadNewBook: UploadNewBook,
  barcodeScan: BarcodeScan
});

const conversationStack = createStackNavigator({
  chatsView: ChatsView,
  chatRoom: ChatRoom,
  ScheduleAppointment:ScheduleAppointment
})

const apointmentStack = createStackNavigator({
  chatRoom: ChatRoom,
  ScheduleAppointment:ScheduleAppointment
})
const searchBookStack = createStackNavigator({
  booksSearch: BooksSearch,
  eCommerceView: ECommerceView
})

conversationStack.navigationOptions = ({ navigation }) => {
  
  if(navigation.state.index == 1){
      return {
          tabBarVisible: false,
      };
  }
  return {
      tabBarVisible: true,
  };
};

uploadedBooksStack.navigationOptions = ({ navigation }) => {
  
  if(navigation.state.index == 1 || navigation.state.index == 2){
      return {
          tabBarVisible: false,
      };
  }
  return {
      tabBarVisible: true,
  };
};

export default createAppContainer(createBottomTabNavigator(
  {
    uploadedBooks: { screen: uploadedBooksStack },
    booksSearch: { screen: searchBookStack },
    booksSelectedToBuy: { screen: BooksSelectedToBuy },
    chatsView: { screen: conversationStack },
    userSettings: { screen: UserSettings },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;

        if (routeName === 'uploadedBooks') {
          return <Image
            source={require('../assets/icons/upload.png')}
          />
        } 
        else if (routeName === 'booksSearch') {
          return <Image
            source={require('../assets/icons/search.png')}
          />
        }
        else if (routeName === 'booksSelectedToBuy') {
          return <Image
            source={require('../assets/icons/order.png')}
          />
        }
        else if (routeName === 'chatsView') {
          return <Image
            source={require('../assets/icons/chat.png')}
          />
        }
        else if (routeName === 'userSettings') {
          return <Image
            source={require('../assets/icons/settings.png')}
          />
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
));
