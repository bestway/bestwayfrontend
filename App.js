/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import SignIn from './src/components/Authentication/SignIn';
import TabsNavigation from './src/utils/TabsNavigation';
import { USER_KEY } from './src/utils/config';
import {
  StyleSheet, 
  AsyncStorage,
  ActivityIndicator,
  StatusBar,
  View,
} from 'react-native';
import {createSwitchNavigator, createAppContainer } from 'react-navigation';

class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem(USER_KEY);

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: TabsNavigation,
    Auth: SignIn,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));